import oasp.DemoApplication;
import oasp.controllers.SonarController;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.web.client.RestTemplate;

import static org.mockito.Mockito.when;

@WebMvcTest(SonarController.class)
@ContextConfiguration(classes = DemoApplication.class)
public class SonarControllerTest {

	@MockBean
	private RestTemplate restTemplate;

	@Autowired
	private MockMvc mockMvc;

	@Test
	public void testGetOwaspTop10() throws Exception {
		// Mock the response from the external API
		String mockApiResponse = "{\"projectKey\":\"project1\",\"owaspVulnerabilities\":[{\"rule\":\"sqli\",\"severity\":\"HIGH\",\"message\":\"SQL Injection\",\"component\":\"component1\",\"line\":10}]}";
		ResponseEntity<String> mockResponse = ResponseEntity.ok(mockApiResponse);
		when(restTemplate.exchange(Mockito.anyString(), Mockito.any(HttpMethod.class), Mockito.any(HttpEntity.class), Mockito.eq(String.class)))
				.thenReturn(mockResponse);

		// Perform the GET request and verify the response
		mockMvc.perform(MockMvcRequestBuilders.get("/api/owasp-top10/{projectKey}", "project1")
						.accept(MediaType.APPLICATION_JSON))
				.andExpect(MockMvcResultMatchers.status().isOk())
				.andExpect(MockMvcResultMatchers.content().json(mockApiResponse));
	}
}
