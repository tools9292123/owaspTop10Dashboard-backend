package oasp.dtos;

import java.util.List;

public class SonarOwaspResponseDTO {

    private String projectKey;
    private List<OwaspVulnerabilityDTO> owaspVulnerabilities;

    // Default constructor
    public SonarOwaspResponseDTO() {
    }

    // Getters and Setters
    public String getProjectKey() {
        return projectKey;
    }

    public void setProjectKey(String projectKey) {
        this.projectKey = projectKey;
    }

    public List<OwaspVulnerabilityDTO> getOwaspVulnerabilities() {
        return owaspVulnerabilities;
    }

    public void setOwaspVulnerabilities(List<OwaspVulnerabilityDTO> owaspVulnerabilities) {
        this.owaspVulnerabilities = owaspVulnerabilities;
    }

    // Inner class representing OWASP vulnerability
    public static class OwaspVulnerabilityDTO {

        private String rule;
        private String severity;
        private String message;
        private String component;
        private int line;

        // Default constructor
        public OwaspVulnerabilityDTO() {
        }

        // Getters and Setters
        public String getRule() {
            return rule;
        }

        public void setRule(String rule) {
            this.rule = rule;
        }

        public String getSeverity() {
            return severity;
        }

        public void setSeverity(String severity) {
            this.severity = severity;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }

        public String getComponent() {
            return component;
        }

        public void setComponent(String component) {
            this.component = component;
        }

        public int getLine() {
            return line;
        }

        public void setLine(int line) {
            this.line = line;
        }
    }
}
