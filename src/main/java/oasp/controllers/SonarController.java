package oasp.controllers;

import oasp.dtos.SonarOwaspResponseDTO;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.*;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

@RestController
@RequestMapping("/api")
public class SonarController {

    @Autowired
    private RestTemplate restTemplate;

    @Value("${sonar.server.url}")
    private String sonarApiUrl;

    @GetMapping("/owasp-top10/{projectKey}")
    public ResponseEntity<SonarOwaspResponseDTO> getOwaspTop10(@PathVariable String projectKey) {
        String sonarApiUrlWithProject = sonarApiUrl + "/api/issues/search?componentKeys=" + projectKey +
                "&rules=sqli,xss,lfi,rfi,csrf,ssrf,dos,idor,redirect,insecure-crypto&statuses=OPEN";
        String accessToken = generateAccessToken(); // Method to generate the access token dynamically
        HttpHeaders headers = new HttpHeaders();
        headers.set("Authorization", "Bearer " + accessToken);
        HttpEntity<String> entity = new HttpEntity<>(headers);
        ResponseEntity<String> response = restTemplate.exchange(sonarApiUrlWithProject, HttpMethod.GET, entity, String.class);

        // Convert the API response to the DTO object
        ObjectMapper objectMapper = new ObjectMapper();
        SonarOwaspResponseDTO responseDTO;
        try {
            responseDTO = objectMapper.readValue(response.getBody(), SonarOwaspResponseDTO.class);
        } catch (JsonProcessingException e) {
            // Handle exception if the response cannot be parsed to the DTO object
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }

        return ResponseEntity.ok(responseDTO);
    }

    private String generateAccessToken() {
        // Logic to generate the access token dynamically
        // You can use a library or implement your own logic to generate the token
        // For example, you can generate a JWT token or retrieve it from a secure storage
        // Return the generated token
        return "";
    }
}
